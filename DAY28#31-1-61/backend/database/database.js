const mysql = require('mysql2/promise')
module.exports = async () => {
  const connect = await mysql.createPool({
    "host": "127.0.0.1",
    "user": "root",
    "password": "root",
    "database": "react"
  })

  return{
    execute(sql,param = []){
      return connect.execute(sql, param)
    }
  }
}