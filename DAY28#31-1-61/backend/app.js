const Koa = require('koa')
const router = require('koa-router')()
const bodyparser = require('koa-bodyparser')
const database = require('./database/database')
const cors = require('@koa/cors');
const main = async () =>{
  const db = await database()
  router
  .post('/', ctx => {
    ctx.body = 'hello ok!'
    console.log(ctx.body + "ok")
  })
  .post('/add', async ctx => {
         let {name} = ctx.request.body
         console.log(name)
        let [result] = await db.execute(`Insert into user (name) values (?)`,[name])
         ctx.body = { name , id:  result.insertId}
      })
 .post('/delete', async ctx => {
        let id = ctx.request.body.id
        await db.execute(`DELETE FROM user WHERE id = ? `,[id])      
 } )
      
.get('/list', async ctx =>{
        let [result] = await db.execute(`select * from user`)
        ctx.body= result
      })
      

      

  const app = new Koa()
  app.use(cors())  
  .use(bodyparser())
  .use(router.routes())
  .listen(4000)
}
main()
